这个项目主要是记录一些我的在线公开课学习笔记，主要包括[Coursera](https://www.coursera.org/)、[Udacity](https://www.udacity.com/)和[EdX](https://www.edx.org/)上的课程。

这里我没有使用传统的[MarkDown]语法进行编辑，而是使用了[Pandoc]，原因在于[Pandoc]可以很好地与$\LaTeX$和[MathJax]进行转换，这样写完以后，我们可以很方便地把结果转换成pdf或带有[MathJax]的HTML页面。

同时也欢迎与[我](http://octman.sinaapp.com)交流。:)

[MarkDown]: http://daringfireball.net/projects/markdown/
[Pandoc]: http://johnmacfarlane.net/pandoc/
[MathJax]: http://www.mathjax.org/