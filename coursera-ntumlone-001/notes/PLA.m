function [iteration] = PLA()
% clear, clc, close all;

load hw1_15_train.dat;
X = hw1_15_train(:, 1:4);
y = hw1_15_train(:, 5);

theta = rand(size(X, 2) + 1, 1);
[m n] = size(X);
X = [ones(m, 1), X];
y(find(y == 0)) = -1;
% plotData(X(:, 2:end), y);
y_new = sign(X * theta);

iter = 5000;

for i = 1 : iter 
    j = 1;
    update = 0;
    for j = 1 : m
        if y_new(j) ~= y(j)
            theta = theta + 0.5*y(j)*X(j, :)';
            update = 1;
            break
        end
    end
    if update == 0 
        update; i;
        break
    end
    y_new = sign(X * theta);
end
iteration = i;
y_new = sign(X * theta);
