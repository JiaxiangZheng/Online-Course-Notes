function [err] = PLA_pocket()
% clear, clc, close all;

load hw1_18_train.dat;
X = hw1_18_train(:, 1:4);
y = hw1_18_train(:, 5);

theta = rand(size(X, 2) + 1, 1);
[m n] = size(X);
X = [ones(m, 1), X];
y(find(y == 0)) = -1;
% plotData(X(:, 2:end), y);
y_new = sign(X * theta);

iter = 100;

theta_best = theta;
error_best = length(y) + 1;

for i = 1 : iter 
    j = 1;
    update = 0;
    perm = randperm(m);
    for j = 1 : m
        if y_new(perm(j)) ~= y(perm(j))
            theta = theta + y(perm(j))*X(perm(j), :)';
            update = 1;
            break
        end
    end
    y_new = sign(X * theta);
    error_cnt = length(y) - sum(y==y_new);
    if error_cnt < error_best
        error_best = error_cnt;
        theta_best = theta;
    end
    if update == 0  %% linear seperatable
        update; i;
        break
    end
end
error_best;

load hw1_18_test.dat;
X = hw1_18_test(:, 1:4);
y = hw1_18_test(:, 5);
[m n] = size(X);
X = [ones(m, 1), X];
err = length(y) - sum((y == sign(X * theta_best)));

