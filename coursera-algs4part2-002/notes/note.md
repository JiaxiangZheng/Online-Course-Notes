stanford大学的算法课程，这是第二学期，比之前的课程的内容更加深入。

### 图基础

第一课是图相关的基础，包括有向图与无向图。同时对图的两种遍历方式，拓扑排序进行了
介绍，同时涉及到了连通分支的概念。这里对拓扑排序和连通分支作简要的笔记。

#### 拓扑排序

拓扑排序要求图为DAG（即有向无环图），执行DFS，并以相反的顺序返回后序，可以通过维
护一个栈达到要求。

代码如下图：

![](figure/dfs_topo.jpg)

考虑任何一条边$v\to{}w$，在调用`dfs(v)`的时候，会发生两种状态，即`dfs(w)`已被调
用并返回或者`dfs(w)`还未调用。对于DAG，只有这两种情况会发生，如果`dfs(w)`已发生
并且未返回，则必然是出现了环。

这里有一个非常有趣的例子，Excel软件中如果第一个元素的计算依赖于第二个元素，第二
个依赖于第三个，第三个依赖于第一个，显然就会构成一个环，是无法计算的。

#### 有向图的强连通分支

定义：如果顶点$v$和顶点$w$之间相互有路径，那么它们就是强连通的。

强连通是一个等价关系，因此可以把一个有向图按照强连通关系划分为多个等价类，即强连
通分支。根据这个定义，DAG的强连通分支为顶点的个数。

计算一个无向图的连通分支只需要调用DFS即可实现，但对于有向图的强连通分支，则不易
设计其算法。Tarjan et al.在1972年的论文中提出了一个线性的DFS方法实现强连通分支的
计算，但其实现难度比较大；相比而言，Kosaraju-Sharir在1980年代提出的两步线性方法
则是一个比较容易实现的方法。Gabow等人进一步提出更简单的线性方法。

Kosaraju-Sharir计算强连通分支的算法如下（复杂度为$O(E+V)$）：   

1. 先运行对$G^R$调用DFS计算后序的反向列表
2. 对原始的图进行DFS遍历，以第1步得到的列表为顺序进行访问，然后每次DFS所经过的结
点属于一个连通分量

其证明如下：

![](figure/kosaraju_proof.jpg)


### 最大流

最小割和最大流问题是等价的。


### Trie


### 字符串匹配

首先，这个问题是非常常见的。具体的字符串匹配有如下方式：

#### 朴素字符串匹配算法

伪代码大致如下：
    
    // return the position of the first occurence in str for pat
    func strMatch(str, pat string) int {
        lstr, lpat := len(str), len(pat)
        for i := 0; i<= lstr-lpat; i++ {
            j := 0
            for ; j<lpat; j++ {
                if lpat[j] != lstr[i+j] {
                    break
                }
            }
            if j == lpat {
                return i
            }
        }
        return -1
    }

朴素方法是最简单直观的，但也是复杂度最高的方法。

#### BM算法

在模式串扫描的过程中，从后向前扫描进行比较。这样，最多可以在一次比较后直接跳过模
式串长度的字符。不过貌似视频和书中讲的是最简单的BM算法，而事实上BM算法的原始论文
比这里讲到的要复杂的多，而且使得最后复杂度能够得到一个非常大的提升。

考虑当前元素$S_{i+j}$与模型串元素$P_j$不匹配的时候（匹配的时候那就继续比较前一个
元素了），有以下几种情况：

1. $S_{i+j}$不出现在模式串$P$中，这时可以直接将模式串的开头移动到$S_{i+j}$之后继续进行比较，即；
2. $S_{i+j}$出现在模式串$P$中，则可以用以下两个方式进行判断：
    * 首先，假定$P$中$S_{i+j}$字符出现的位置在$j$之前的位置$k$，则可以将模式串后移到使得$S_{i+j}$与$P_k$中对齐；
    * 另外，考虑到$P_j$之后的这部分后缀$P(j:]$已经匹配，称该后缀为好后缀，如果后缀在$P$的前面还出现了，那么可以考虑直接将该后缀再次对齐。   
显然，这两种策略都是适合的，可以选择跳跃步数更大的那个。

C语言代码如下：

    #include <stdint.h>
    #include <stdlib.h>

    #define ALPHABET_LEN 256
    #define NOT_FOUND patlen
    #define max(a, b) ((a < b) ? b : a)

    // delta1 table: delta1[c] contains the distance between the last
    // character of pat and the rightmost occurence of c in pat.
    // If c does not occur in pat, then delta1[c] = patlen.
    // If c is at string[i] and c != pat[patlen-1], we can
    // safely shift i over by delta1[c], which is the minimum distance
    // needed to shift pat forward to get string[i] lined up 
    // with some character in pat.
    // this algorithm runs in alphabet_len+patlen time.
    void make_delta1(int *delta1, uint8_t *pat, int32_t patlen) {
        int i;
        for (i=0; i < ALPHABET_LEN; i++) {
            delta1[i] = NOT_FOUND;
        }
        for (i=0; i < patlen-1; i++) {
            delta1[pat[i]] = patlen-1 - i;
        }
    }
     
    // true if the suffix of word starting from word[pos] is a prefix 
    // of word
    int is_prefix(uint8_t *word, int wordlen, int pos) {
        int i;
        int suffixlen = wordlen - pos;
        // could also use the strncmp() library function here
        for (i = 0; i < suffixlen; i++) {
            if (word[i] != word[pos+i]) {
                return 0;
            }
        }
        return 1;
    }
     
    // length of the longest suffix of word ending on word[pos].
    // suffix_length("dddbcabc", 8, 4) = 2
    int suffix_length(uint8_t *word, int wordlen, int pos) {
        int i;
        // increment suffix length i to the first mismatch or beginning
        // of the word
        for (i = 0; (word[pos-i] == word[wordlen-1-i]) && (i < pos); i++);
        return i;
    }
     
    // delta2 table: given a mismatch at pat[pos], we want to align 
    // with the next possible full match could be based on what we
    // know about pat[pos+1] to pat[patlen-1].
    //
    // In case 1:
    // pat[pos+1] to pat[patlen-1] does not occur elsewhere in pat,
    // the next plausible match starts at or after the mismatch.
    // If, within the substring pat[pos+1 .. patlen-1], lies a prefix
    // of pat, the next plausible match is here (if there are multiple
    // prefixes in the substring, pick the longest). Otherwise, the
    // next plausible match starts past the character aligned with 
    // pat[patlen-1].
    // 
    // In case 2:
    // pat[pos+1] to pat[patlen-1] does occur elsewhere in pat. The
    // mismatch tells us that we are not looking at the end of a match.
    // We may, however, be looking at the middle of a match.
    // 
    // The first loop, which takes care of case 1, is analogous to
    // the KMP table, adapted for a 'backwards' scan order with the
    // additional restriction that the substrings it considers as 
    // potential prefixes are all suffixes. In the worst case scenario
    // pat consists of the same letter repeated, so every suffix is
    // a prefix. This loop alone is not sufficient, however:
    // Suppose that pat is "ABYXCDEYX", and text is ".....ABYXCDEYX".
    // We will match X, Y, and find B != E. There is no prefix of pat
    // in the suffix "YX", so the first loop tells us to skip forward
    // by 9 characters.
    // Although superficially similar to the KMP table, the KMP table
    // relies on information about the beginning of the partial match
    // that the BM algorithm does not have.
    //
    // The second loop addresses case 2. Since suffix_length may not be
    // unique, we want to take the minimum value, which will tell us
    // how far away the closest potential match is.
    void make_delta2(int *delta2, uint8_t *pat, int32_t patlen) {
        int p;
        int last_prefix_index = patlen-1;

        // first loop
        for (p=patlen-1; p>=0; p--) {
            if (is_prefix(pat, patlen, p+1)) {
                last_prefix_index = p+1;
            }
            delta2[p] = last_prefix_index + (patlen-1 - p);
        }
     
        // second loop
        for (p=0; p < patlen-1; p++) {
            int slen = suffix_length(pat, patlen, p);
            if (pat[p - slen] != pat[patlen-1 - slen]) {
                delta2[patlen-1 - slen] = patlen-1 - p + slen;
            }
        }
    }
     
    uint8_t* boyer_moore (uint8_t *string, uint32_t stringlen, uint8_t *pat, uint32_t patlen) {
        int i;
        int delta1[ALPHABET_LEN];
        int *delta2 = (int *)malloc(patlen * sizeof(int));
        make_delta1(delta1, pat, patlen);
        make_delta2(delta2, pat, patlen);

        i = patlen-1;
        while (i < stringlen) {
            int j = patlen-1;
            while (j >= 0 && (string[i] == pat[j])) {
                --i;
                --j;
            }
            if (j < 0) {
                free(delta2);
                return (string + i+1);
            }
     
            i += max(delta1[string[i]], delta2[j]);
        }
        free(delta2);
        return NULL;
    }

