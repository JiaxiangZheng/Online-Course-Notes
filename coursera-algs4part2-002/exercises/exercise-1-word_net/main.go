package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
	"time"
)

func OpenFile(path string) *os.File {
	file, err := os.Open(path)
	if err != nil {
		fmt.Println(err)
		os.Exit(-1)
	}
	return file
}

type Synset struct {
	word    []string
	comment string
}

func ReadSynset(filename string) map[int]Synset {
	r := bufio.NewReader(OpenFile(filename))
	dict := make(map[int]Synset, 100)
	var synset Synset
	for {
		line, _, err := r.ReadLine()
		if err != nil {
			break
		}
		record := strings.Split(string(line), ",")
		synset.comment = record[2]
		synset.word = strings.Split(record[1], " ")
		id, _ := strconv.Atoi(record[0])
		dict[id] = synset
	}
	return dict
}

func ConstructHyper(map[int]Synset, filename string) {

}
func main() {
	tic := time.Now()
	dict := ReadSynset("input/synsets.txt")
	fmt.Println("finished reading file, time usage : ", time.Now().Sub(tic))
	fmt.Println(len(dict))
	return
}
